<?php

error_reporting(E_ALL & ~E_NOTICE);
require_once('mysql/mysql.class.php');
require_once('rskmorph.php');
require_once('marathon.php');
require_once('lines_config.php');
define(DOMAIN_PREFIX, '/rsklines/');
define(IMAGE_PATH, 'images/');
define(MAIN_TEXT_FONT_SIZE, 10);

if (isset($_FILES['add_image']) && !empty($_FILES['add_image']))
{

    $name     = current($_FILES['add_image']['name']);
    $tmp_name = current($_FILES['add_image']['tmp_name']);
    $size     = current($_FILES['add_image']['size']);
    $type     = current($_FILES['add_image']['type']);
    $error    = current($_FILES['add_image']['error']);
    $file     = array('name' => $name, 'tmp_name' => $tmp_name, 'size' => $size, 'error' => $error, 'type' => $type);
    $mime     = $file['type'];
    $size     = $file['size'];
    if (!in_array($mime, array('image/gif', 'image/png', 'image/jpeg')))
    {
        die('Ошибка. Неправильный формат файла. Разрешаются gif, png, jpeg');
    }
    if ($size > 1024 * 1024 * 5)
    {
        die('Ошибка. Слишком большой файл. Разрешается загружать не больше 5 Мб ');
    }
    $pinfo       = pathinfo($file['name'], PATHINFO_EXTENSION);
    $destination = 'uploads';

    $new_fname_full = IMAGE_PATH . $file['name'];
    if (move_uploaded_file($file['tmp_name'], $new_fname_full))
    {
        echo $new_fname_full;
    }
    else
    {
        die('Ошибка. Не удалось скопировать файл');
    }
}
else
{
    $row      = $_REQUEST;
    $marathon = new Marathon();
    if (!empty($row['id']))
    {
        $marathon->loadMarathon($row['id']);
        $old_picture = $marathon->resulting_image;
    }

    if (isset($_REQUEST['purpose']) && !empty($_REQUEST['purpose']) && !isset($_REQUEST['raw']) &&
            ('make_line' == $_REQUEST['purpose'] || 'refresh_line' == $_REQUEST['purpose']))
    {
        $marathon->loadFromArray($_POST);
        $marathon->id = $marathon->saveMarathon();
        if (!$marathon->id)
        {
            $marathon->getError();
            die('Ошибка сохранения');
        }

        $image_wizard              = new MarathonImageWizard($marathon);
        $marathon->resulting_image = $image_wizard->getImg();
        $marathon->saveMarathon();
        unset($marathon->dbconn);
        if (!empty($old_picture))
        {
            @unlink($old_picture);
        }
        echo json_encode($marathon);
    }
    if (isset($_REQUEST['purpose']) && !empty($_REQUEST['purpose']) && isset($_REQUEST['raw']))
    {

        $image_wizard              = new MarathonImageWizard($marathon);
        $marathon->resulting_image = $image_wizard->getImg();
        $marathon->saveMarathon();
        unset($marathon->dbconn);
        echo json_encode($marathon);
        if (isset($_GET['raw']))
        {
            ob_end_clean();
            ob_start();
            //echo $marathon->resulting_image;
            $im = imagecreatefrompng('' . $marathon->resulting_image);
            header('Content-Type: image/png');
            imagepng($im);
            imagedestroy($im);
            ob_end_flush();
        }
    }
}
?>
