<?php
require_once('lines_config.php');
require_once('marathon.php');
$domain_prefix = '/rsklines/';
$mar_length    = 100;
$template_text = 'До конца стодневки осталось {$remains} дней';
$marathons     = new Marathons();
$rows          = $marathons->getMarathons();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Freelancer - Start Bootstrap Theme</title>

        <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
        <link href="<?= $domain_prefix ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= $domain_prefix ?>css/freelancer.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->        
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="<?= $domain_prefix ?>js/jquery.js"></script>

        <script>
            function uploadImage(idx) {
                var content = null, iframe = $('iframe[name="uploadTrg[' + idx + ']"').get(0);
                if (iframe.contentDocument) {
                    content = iframe.contentDocument.body.innerHTML;
                } else if (iframe.contentWindow) {
                    content = iframe.contentWindow.document.body.innerHTML;
                } else if (iframe.document) {
                    content = iframe.document.body.innerHTML;
                }
                $('*[id="preview[0]"').attr('src', content);

            }
        </script>
    </head>

    <body id="page-top" class="index">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Навигация</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#page-top">Старт стодневок</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li class="page-scroll">
                            <a href="#ndays">Стодневки</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#about">Обо мне</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#contact">Контакты</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <!-- Header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">

<!-- <img class="img-responsive" src="img/profile.png" alt="">
<div class="intro-text">
   <span class="name">Start Bootstrap</span>
   <hr class="star-light">
   <span class="skills">Web Developer - Graphic Artist - User Experience Designer</span>
</div>-->
                    </div>
                </div>
            </div>
        </header>

        <!-- Ndays Grid Section -->
        <section id="ndays">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Стодневки</h2>
                        <hr class="star-primary">
                    </div>
                </div>
                <?php
                if ($rows->num_rows > 0):
                    foreach ($rows as $key => $row):
                        $result_links =  !empty($row['resulting_image']) ?
                        ('Файл :'.$row['resulting_image'].', ссылка на линейку: <a target="_blank" href="http://'.$_SERVER['HTTP_HOST'].$domain_prefix.'img'.$row['id'].'.jpg">"http://'.$_SERVER['HTTP_HOST'].$domain_prefix.'img'.$row['id'].'.jpg"</a>') : '';
                        ?>
                        <section class="marathon-section" id="nday_<?=$key?>">
                            <div class="container marathon-container">
                                <header >
                                    
                                    <div style="float:left">
                                        <span class="badge-id badge badge-success">ID:<?= $row['id'] ?></span>
                                        <input type="hidden" name="id[<?= $row['id'] ?>]" value="<?=$row['id']?>" />
                                    </div>
                                    <label for="marathon_name_<?= $key ?>">Название марафона</label>                                    
                                    <input style="color:#000" name="marathon_name[<?= $key ?>]" id="marathon_name_<?= $key ?>" value="<?= $row['marathon_name'] ?>" size="48"/>
                                </header>					
                                <div class="row">
                                    
                                    <div class="col-md-4 col-lg-4" style="padding-top:25px">
                                        <form  action="action.php" name="upload_form[<?= $key ?>]" method="post" enctype="multipart/form-data" target="uploadTrg[<?= $key ?>]"> 
                                            <input type="file" class="form-control" name="add_image[<?= $key ?>]" id="add_image_<?= $key ?>"/>
                                            <!-- <input type="hidden" name="purpose[<?= $key ?>]" value="upload" /> -->
                                        </form>
                                        <iframe id="uploadTrg[<?= $key ?>]" my-required="1" name="uploadTrg[<?= $key ?>]" height="0" width="0" frameborder="0" scrolling="yes" onload="uploadImage(<?= $key ?>)"></iframe>
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                        <label for="start_date[<?= $key ?>]">Начало марафона</label>                                        
                                        <input type="date" class="form-control" class="start-date" my-required="1" name="start_date[<?= $key ?>]" value="<?= date('Y-m-d', strtotime($row['marathon_start'])) ?>"/>
                                    </div>
                                    <div class="col-md-2 col-lg-2">
                                        <label for="marathon_number">№ марафона</label>
                                        <input type="number" class="form-control" my-required="1" min="1" max="500" name="marathon_number[<?= $key ?>]" id="marathon_number_<?=$key?>" value="<?= $row['marathon_number'] ?>"/>
                                    </div>
                                    <div class="col-md-2 col-lg-2">                                                        
                                        <label for="ndays_count">Длительность, дней</label>
                                        <input type="number" class="form-control" my-required="1" min="1" max="500" name="ndays_count[<?= $key ?>]" id="ndays_count_<?=$key?>" value="<?= $row['length'] ?>"/>
                                    </div>
                                </div>
                                <div class="row"> 	
                                    <div class="col-md-4 col-lg-4">                                
                                        <label for="occupation">Должность</label>
                                        <?php
                                        $val = intval($row['position']);
                                        ?>
                                        <select id="occupation_<?= $key ?>" class="form-control sel-occupation" name="occupation[<?= $key ?>]"  style="width:300px">
                                            <option <?= empty($val)  ? "selected" : "" ?> value="0"></option>
                                            <option <?= $val == 1 ? "selected" : "" ?> value="1">Капитан</option>
                                            <option <?= $val == 2 ? "selected" : "" ?> value="2">Заместитель капитана</option>
                                            <option <?= $val == 3 ? "selected" : "" ?> value="3">Мудрец</option>
                                            <option <?= $val == 4 ? "selected" : "" ?> value="4">Помощник рефери</option>
                                            <option <?= $val == 5 ? "selected" : "" ?> value="5">Мотиваторник</option>
                                            <option <?= $val == 6 ? "selected" : "" ?> value="6">Демотиваторник</option>
                                            <option <?= $val == 7 ? "selected" : "" ?> value="7">Позитивник</option>
                                            <option  <?= $val == 7 ? "selected" : "" ?>  value="8">Другое...</option>
                                        </select>

                                    </div>
                                    <div class="col-md-4 col-lg-4 other_occ_container" style="<?= $val == 8 ? '' : 'display:none;' ?> "> 
                                        <label for="other_occupation[<?= $key ?>]">Другая должность</label>
                                        <input type="text" class="form-control other_occupation" id="other_occupation_<?= $key ?>" name="other_occupation[<?= $key ?>]" style=" width:300px;"/>
                                    </div>
                                    <div class="col-md-4 col-lg-4 team" > 
                                        <label for="team[<?= $key ?>]">Команда</label>
                                        <input type="text" class="form-control team" id="team_<?= $key ?>" name="team[<?= $key ?>]" value="<?=$row['team']?>" style="width:300px;"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-lg-8">
                                        <label for="text_template">Текст в подписи </label>
                                        <input type="text" class="form-control" name="text_template[<?= $key ?>]" id="text_template_<?=$key?>" value="<?= $row['text_template']?>"/>
                                    </div>                        
                                </div>
                                <div class="row" style="text-align:center">
                                    <div class="col-md-2 col-lg-2">
                                        <button id="send[<?= $key ?>]"> Получить линейку </button>
                                    </div>
                                    <div class="col-md-8 col-lg-8">
                                        <img class="preview" id="preview_<?= $key ?>" style="width:302px;height:32px;" src="<?= $row['template_path'] ?>"/>
                                        <img class="postperform" id="postperform_<?= $key ?>" style="width:302px;height:32px;" src="<?= $row['resulting_image'] ?>"/>                                      
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-lg-10"> 
                                        <span id="post-perform-url_<?= $key ?>" class="text-info post-perform-url"><?=$result_links?></span>
                                    </div>
                                </div>
                            </div>
                        </section>                
                        <?php
                    endforeach;
                    echo '<button id="clone">Создать еще одну линейку</button>';
                else:
                    $key = 0;
                    ?>
                    <section id="nday_0">				
                        <div class="container marathon-container">
                            <header>
                                 <div style="float:left">
                                        <span class="badge-id badge badge-success">ID:<?= $row['id'] ?></span>
                                        <input type="hidden" name="id[<?= $key ?>]" value="<?=$key?>" />
                                </div>
                                <label for="marathon_name_<?= $key ?>">Название марафона</label>
                                <input style="color:#000" name="marathon_name[<?= $key ?>]" id="marathon_name_<?= $key ?>" value="Марафон" size="48"/>                                
                            </header>					
                            <div class="row">
                                <input type="hidden" name="id[<?= $key ?>]" value="0"/>
                                <div class="col-md-4 col-lg-4" style="padding-top:25px">
                                    <form  action="action.php" name="upload_form[<?= $key ?>]" method="post" enctype="multipart/form-data" target="uploadTrg[<?= $key ?>]"> 
                                        <input type="file" class="form-control" name="add_image[<?= $key ?>]" id="add_image_<?= $key ?>"/>                                        
                                    </form>
                                    <iframe id="uploadTrg[<?= $key ?>]" my-required="1" name="uploadTrg[<?= $key ?>]" height="0" width="0" frameborder="0" scrolling="yes" onload="uploadImage(<?= $key ?>)"></iframe>
                                </div>
                                <div class="col-md-2 col-lg-2">                                    
                                    <label for="start_date[<?= $key ?>]">Начало марафона</label>                                        
                                        <input type="date" class="form-control" class="start-date" my-required="1" name="start_date[<?= $key ?>]" value="<?= date('Y-m-d') ?>"/>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <label for="marathon_number">№ марафона</label>
                                    <input type="number" class="form-control" my-required="1" min="1" max="500" name="marathon_number[<?= $key ?>]" id="marathon_number_<?=$key?>" value="<?= $row['marathon_number'] ?>"/>
                                </div>
                                <div class="col-md-3 col-lg-3">                                                        
                                    <label for="ndays_count">Длительность марафона, дней</label>
                                    <input type="number" class="form-control" my-required="1" min="1" max="500" name="ndays_count[<?= $key ?>]" id="ndays_count_<?=$key?>" value="100"/>
                                </div>
                            </div>
                            <div class="row"> 	
                                <div class="col-md-4 col-lg-4">                                
                                    <label for="occupation">Должность</label>
                                    <?php
                                    $val = intval($row['position']);
                                    ?>
                                    <select id="occupation_<?= $key ?>" class="form-control sel-occupation" name="occupation[<?= $key ?>]"  style="width:300px">
                                        <option <?= empty($val)  ? "selected" : "" ?> value="0"></option>
                                        <option <?= $val == 1 ? "selected" : "" ?> value="1">Капитан</option>
                                        <option <?= $val == 2 ? "selected" : "" ?> value="2">Заместитель капитана</option>
                                        <option <?= $val == 3 ? "selected" : "" ?> value="3">Мудрец</option>
                                        <option <?= $val == 4 ? "selected" : "" ?> value="4">Помощник рефери</option>
                                        <option <?= $val == 5 ? "selected" : "" ?> value="5">Мотиваторник</option>
                                        <option <?= $val == 6 ? "selected" : "" ?> value="6">Демотиваторник</option>
                                        <option <?= $val == 7 ? "selected" : "" ?> value="7">Позитивник</option>
                                        <option  <?= $val == 7 ? "selected" : "" ?>  value="8">Другое...</option>
                                    </select>

                                </div>
                                <div class="col-md-4 col-lg-4 other_occ_container" style="<?= $val == 8 ? '' : 'display:none;' ?> "> 
                                    <label for="other_occupation[<?= $key ?>]">Другая должность</label>
                                    <input type="text" class="form-control other_occupation" id="other_occupation[<?= $key ?>]" name="other_occupation[<?= $key ?>]" style=" width:300px;"/>
                                </div>
                                <div class="col-md-4 col-lg-4 team" > 
                                    <label for="team[<?= $key ?>]">Команда</label>
                                    <input type="text" class="form-control team" id="team_<?= $key ?>" name="team[<?= $key ?>]" style="width:300px;"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8">
                                    <label for="text_template">Текст в подписи </label>
                                    <input type="text" class="form-control" name="text_template[<?= $key ?>]" id="text_template_<?=$key?>" value="<?= $text_template ?>"/>
                                </div>                        
                            </div>
                            <div class="row" style="text-align:center">
                                <div class="col-md-2 col-lg-2">
                                    <button id="send[<?= $key ?>]"> Получить линейку </button>
                                </div>
                                <div class="col-md-8 col-lg-8">
                                    <img class="preview" id="preview_<?= $key ?>" style="width:302px;height:32px;" src="uploads/100dney.png"/>
                                    <img class="postperform" id="postperform_<?= $key ?>" style="width:302px;height:32px;" src="<?= $row['resulting_image'] ?>"/>                                      
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <span id="post-perform-url_<?= $key ?>" class="text-success post-perform-url"> </span>
                                </div>
                            </div>
                        </div>
                    </section>
                    <button id="clone">Создать еще одну линейку.</button>
                <?php
                endif;
                ?>
            </div>
        </section>
        <!-- About Section -->


        <!-- Contact Section -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3>Свяжитесь со мной</h3>
                        <hr class="star-primary">                        
                    </div>
                </div>

            </div>

        </section>

        <!-- Footer -->
        <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-4">
                            <h3>***</h3>
                            <p>****<br>***</p>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>Я в Web</h3>
                            <ul class="list-inline">
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>Фрилансер</h3>

                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright &copy; Мой сайт
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
        <div class="scroll-top page-scroll visible-xs visible-sm">
            <a class="btn btn-primary" href="#page-top">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>

        <!-- Portfolio Modals -->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>Project Title</h2>
                                <hr class="star-primary">
                                <img src="img/portfolio/cabin.png" class="img-responsive img-centered" alt="">
                                <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                                <ul class="list-inline item-details">
                                    <li>Client:
                                        <strong><a href="http://startbootstrap.com">Start Bootstrap</a>
                                        </strong>
                                    </li>
                                    <li>Date:
                                        <strong><a href="http://startbootstrap.com">April 2014</a>
                                        </strong>
                                    </li>
                                    <li>Service:
                                        <strong><a href="http://startbootstrap.com">Web Development</a>
                                        </strong>
                                    </li>
                                </ul>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      

        <!-- Bootstrap Core JavaScript -->
        <script src="<?= $domain_prefix ?>js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="<?= $domain_prefix ?>js/classie.js"></script>
        <script src="<?= $domain_prefix ?>js/cbpAnimatedHeader.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="<?= $domain_prefix ?>js/jqBootstrapValidation.js"></script>
        <script src="<?= $domain_prefix ?>js/contact_me.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?= $domain_prefix ?>js/freelancer.js"></script>
        <!-- a bit of ALEXRSK Interfaces  -->
        <script src="<?= $domain_prefix ?>js/interface-rsk.js"></script>
    </body>

</html>
