/*!
 * Start Bootstrap - Freelancer Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function () {
    $('body').on('click', '.page-scroll a', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    $("input[name^='add_image']").change(function (event) {
        var el = event.currentTarget, idx = /\[(\d+)\]/.exec($(el).attr('name'))[1];
        $('form[name="upload_form[' + idx + ']"]').get(0).submit();
    });
});

// Floating label headings for the contact form
$(function () {
    $("body").on("input propertychange", ".floating-label-form-group", function (e) {
        $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function () {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function () {
        $(this).removeClass("floating-label-form-group-with-focus");
    });

    $('button[id^="send"]').click(function (event) {
        var container = $(event.currentTarget).parents('div.marathon-container'), preview = $(container).find('img.preview'), 
        postperform = $(container).find('img.postperform'), postperformurl =  $(container).find('span.post-perform-url');
                form_data = collectFormVariables(container);
        for (var key in form_data) {
            var new_key = key.replace(/\[\d+\]/, '');
            form_data[new_key] = form_data[key];
            delete form_data[key];
        }
        delete form_data['add_image'];
        form_data['image_path'] = $(preview).attr('src');        
        if (typeof (form_data['id']) != "undefined" && 0 != form_data['id'] && !isNaN(parseInt(form_data['id']))) {
            form_data['purpose'] = 'refresh_line';
        } else
        {
            form_data['purpose'] = 'make_line';
        }

        var ajaxData = {
            'url': '/rsklines/action.php',
            'data': form_data,
            'type': 'POST'
        };
        $.ajax(ajaxData).complete(function (response) {
            var obj = JSON.parse(response.responseText);
            var id_ctl = $(container).find('input[name^="id"]');
            $(postperform).attr('src', obj.resulting_image.replace(/\'/g,''));
            $(postperformurl).html(obj.resulting_image);
            $(id_ctl).val(obj.id);
           
        });
        return false;

    });

    $('.sel-occupation').change(function (event) {
        var el = $(event.currentTarget), other_occ = $(el).parents('div.marathon-container').find('.other_occ_container');
        if ($(this).val() == 8) {
            $(other_occ).show();
        } else {
            $(other_occ).hide();
        }
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function () {
    $('.navbar-toggle:visible').click();
});
