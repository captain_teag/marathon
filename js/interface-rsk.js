
/**
 * Показывает автокомплит
 * @param dockEl - Стыкующийся элемент
 * @param list - список значений
 * @param click_cb - колбэк при щелчке
 * @param field - Поле, которое показываем
 * @param specflag - Специальный флаг,задающий вид элементов списка. Для 
 *	пользовательской обработки передавайте custom
 * @param customItemHandler - Пользовательская функция, обработчик элемента списка вида function(item){return string}
 * */
AutocompleteOnTheFly = function (dockEl, list, click_cb, fields, specflag, customItemHandler) {
    specflag = specflag || 'trivial';
    $('ul.autocomplete').remove();
    var multifields = false;
    var ul = $('<ul class="autocomplete"/>');
    var val = $('<input type="hidden" name="sel_id" value=""/>').appendTo($('form[name="new_order"]'));
    if (typeof (fields) === 'object' && fields) {
        var multifields = true;
        var field = fields[0];
    } else {
        var field = fields || 'name';
    }
    if (list.length == 0) {
        return false;
    }
    for (var l_item in list) {
        var part_other = list[l_item].name_other ? '&nbsp;<span class="label" style="color:#fff;font-size:14px">' + list[l_item].name_other + '<span>' : '';

        switch (specflag) {
            case 'neworder':
                if (1 != list[l_item].is_complex) {
                    var list_item_text = '[' + list[l_item].code + ']' + ' - ' + list[l_item].name + part_other;
                } else {
                    var list_item_text =
                            '<span class="complex-service">[' + list[l_item].code + ']' + ' - ' + list[l_item].name + '</span>';
                }
                break;
            case 'trivial':
                var list_item_text = list[l_item][field].replace(/\\/g, '');
                break;
            case 'custom':
                var list_item_text = customItemHandler(list[[l_item]]);
                break;
            default:
                var list_item_text = '[' + list[l_item].code + ']' + ' - ' + list[l_item].name;

        }
        if (!multifields) {
            var elem = $('<li data-val="' + list[l_item].id + '" class="ac_item">' + list_item_text + '</li>');
        } else
        {
            var data_part = '';
            for (var j in fields) {
                data_part += ' data-' + fields[j].toString() + '="' + list[l_item][fields[j]] + '"';
            }
            var elem = $('<li' + data_part + ' class="ac_item"> ' + list_item_text + '</li>');
        }
        $(ul).append($(elem).click(click_cb));
    }
    $(ul).css(
            {'top': $(dockEl).position().top + $(dockEl).height() - 10,
                'left': $(dockEl).position().left,
                'width': $(dockEl).width(),
                'z-index': 2000, 'position': 'absolute'
            }
    ).appendTo($(dockEl).parents('div').get(0)).show();

};

AutocompleteTable = function (dock_el, field_name, table_name, autocomplete_id) {
    var value = $(dock_el).val(), ajaxParams = {
        'url': 'index.php?AjaxRequest=getTableDetails',
        'data': {
            'table_name': table_name,
            'params': '{"' + field_name + '":"' + value + '"}',
            'flags': '{"' + field_name + '": "text_search"}',
            'distinct': 1,
        },
        'type': 'POST'
    };

    if (value.length < 3) {
        return false;
    }

    $.ajax(ajaxParams).complete(function (response) {
        var list = JSON.parse(response.responseText),
                autoc_id = autocomplete_id || 'autocomplete',
                cb = function (event) {
                    var event_data = {'value': $(event.currentTarget).data('val'), 'text': $(event.currentTarget).text()};
                    $(dock_el).trigger(autoc_id + ':select', event_data);
                    $('.autocomplete').remove();
                };

        return new AutocompleteOnTheFly(dock_el, list, cb, field_name, 'trivial');
    });
};


getTableDetails = function (table_name, where, callback) {
    var ajaxParams = {
        'url': 'index.php?AjaxRequest=getTableDetails',
        'data': {
            'table_name': table_name,
            'distinct': 1,
        },
        'type': 'POST'
    };
    var params = {}, flags = [];
    if (where) {
        for (var i in where) {
            params[i] = where[i];
            //flags[where[i].name] = 'textsearch';
        }
    }
    ajaxParams.data['params'] = JSON.stringify(params);
   // ajaxParams['flags'] = JSON.stringify(flags);
    return $.ajax(ajaxParams).complete(function (response) {
        var list = JSON.parse(response.responseText);
        if (callback) {
            return callback(list);
        } else {
            return list;
        }
    });
}

tabularizeObjArray = function (objArray, captions, panel, title) {
    var result = $('<table class="tabularized_JSON"><caption>' + title + '</caption></table>');
    for (var i = 0; i < objArray.length; i++) {
        var obj = objArray[i];
        result.append(tabularizeJSON(obj, captions, panel));
    }
    return result;
};

tabularizeJSON = function (obj, captions, prefix, title) {
    var result = $('<table class="tabularized_JSON"></table>');
    if (title) {
        $(result).append($('<div style="font-weight:bold;border:1px solid black;width:90%">' + title + '</div>'));
    }
    //or (var i = 0; i < objarray.length; i++) {
    //		var obj = objarray[i];

    prefix = prefix || "";
    for (var item in obj) {

        if (typeof (captions[item]) != "undefined") {
            var content = captions[item];
        } else {
            content = '<input type="hidden" name="' + prefix + '[' + item + ']' + '" value= "' + obj[item] + '">';
            //var capt = '['+item+']';
            //continue;
        }
        var row = $('<tr></tr>');
        if (obj[item] !== null && typeof (obj[item]) === 'object') {
            var cell = $('<td colspan="2"><small></small></td>').append(tabularizeInstanceArray(obj[item], captions, 'sub'));
        } else {
            if (content.indexOf("<input") === -1) {
                var cell = $('<td style="width:30%">' + content + '</td><td>' + (obj[item] != null ? obj[item] : '') + '</td>');
            } else {
                //var cell = $('<td style="display:hidden" colspan="2">' + content + '</td>');
                cell = null;
            }
        }
        if (cell != null) {
            row.append(cell);
            result.append(row);

        }

    }
//		}
    return $(result);

};

tabularizeInstanceArray = function (objarr, captions, prefix, panel, title) {
    prefix = prefix || '';
    var result = $('<div class="' + prefix + 'tabularize-results-container"></div>');
    if (title) {
        $(result).append('<div style="font-weight:bold;border:1px solid black;width:89%">' + title + '</div>')
    }
    if (objarr == null) {

        return result;
    }
    for (var i = 0; i < objarr.length; i++) {
        var obj = objarr[i].mRow || objarr[i];
        result.append(tabularizeJSON(obj, captions, panel));
    }
    return result;
};


tabularizeRows = function (objarr, captions, panel, title) {
    var result = $('<div class="tabularize-results-container"></div>');
    if (title) {
        $(result).append($('<div style="font-weight:bold;border:1px solid black;width:89%">' + title + '</div>'));
    }
    if (objarr == null) {

        return result;
    }
    var objarr = objarr.mRows;
    for (var i = 0; i < objarr.length; i++) {
        var obj = objarr[i];
        result.append(tabularizeJSON(obj, captions, panel));
    }
    return result;
};


showDialogOK = function (title, str, callbackOK, targetEl, params) {
    params = params || {};
    var positionX = params.x || ~~(screen.width / 3);
    targetEl = targetEl || 'body';
    var darker = $('<div class="darker"/>')
            .prependTo(targetEl)
            .show();
    var positionY = params.y || ~~(screen.height / 2);
    var dialog = $(
            '<div id="dialog" class="ui-jqgrid ui-widget draggable ui-widget-content ui-corner-all" style="position:absolute; z-index:999999; \n\
        min-width:300px; max-height:190px; left:' + positionX + 'px; top:' + positionY + 'px"/>')
            .append($(
                    '<div class="ui-jqgrid-titlebar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"></div>')
                    .append('<span>' + title + '</span>'))
            .append($('<div class="dialog-content" style="padding:10px; background-color:#fff">' + str + '</div>'))
            .append(
                    $(
                            '<div class="button-row" style="text-align:center"><button id="dialogOK">Закрыть</button></div>')
                    );
    if (targetEl == 'body') {
        dialog.appendTo($('#modal'));
    }
    else {
        dialog.appendTo($(targetEl));
    }
    this.dialog = dialog;
    var self = this;

    $(dialog).show();
    $(document).on('click', '#dialogOK', function () {
        $(dialog).remove();
        $(darker).remove();
        callbackOK(self);
    });
};

showDialogOKCancel = function (title, str, callbackOK, callbackCancel, targetEl, params) {
    params = params || {};
    var positionX = params.x || ~~(screen.width / 3);
    targetEl = targetEl || 'body';
    var darker = $('<div class="darker"/>')
            .prependTo(targetEl)
            .show();
    var positionY = params.y || ~~(screen.height / 2);
    var dialog = $(
            '<div id="dialog" class="ui-jqgrid ui-widget draggable ui-widget-content ui-corner-all" style="position:absolute; z-index:10000; \n\
        min-width:300px; max-height:200px; left:' + positionX + 'px; top:' + positionY + 'px"/>')
            .append($(
                    '<div class="ui-jqgrid-titlebar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"></div>')
                    .append('<span>' + title + '</span>'))
            .append($('<div class="dialog-content" style="padding:10px; background-color:#fff">' + str + '</div>'))
            .append(
                    $(
                            '<div class="button-row" style="text-align:center"><button id="dialogOK">Да</button>&nbsp;&nbsp;&nbsp<button id="dialogCancel">Отмена</button></div>')
                    );
    if (targetEl == 'body') {
        dialog.appendTo($('#modal'));
    }
    else {
        dialog.appendTo($(targetEl));
    }
    this.dialog = dialog;
    var self = this;

    $(dialog).show();
    $(document).on('click', '#dialogOK', function () {
        callbackOK(self);
        $(dialog).hide();
        $(darker).hide();
    });

    $(document).on('click', '#dialogCancel', function () {
        callbackCancel(self);
        $(dialog).hide();
        $(darker).hide();
    });
};

showDialogManyBtns = function (title, str, buttons, targetEl, params) {
    this.params = params || {};
    this.buttons = buttons || [{'id': 'OK', 'caption': 'OK', 'callback': function () {
            }}];
    this.title = title;
    this.str = str;
    this.targetEl = targetEl || 'body';
    this.callbacks = [];


};

showDialogManyBtns.prototype.show = function () {

    var positionX = this.params.x || ~~(screen.width / 3);
    var positionY = this.params.y || ~~(screen.height / 2);
    var buttonPanel = $('<div class="button-row" style="text-align:center"></div>');
    for (var i = 0; i < this.buttons.length; i++) {
        var btn = this.buttons[i];
        this.callbacks[btn.id] = btn.callback;
        buttonPanel.append($('<span style="padding:5px"><button id="' + btn.id + '">' + btn.caption + '</button></span>'));
    }
    var darker = $('<div class="darker"/>')
            .prependTo(this.targetEl)
            .show();

    var dialog = $(
            '<div id="dialog" class="ui-jqgrid ui-widget draggable ui-widget-content ui-corner-all" style="position:absolute; z-index:10000; \n\
        min-width:300px; max-height:150px; left:' + positionX + 'px; top:' + positionY + 'px"/>')
            .append($(
                    '<div class="ui-jqgrid-titlebar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"></div>')
                    .append('<span>' + this.title + '</span>'))
            .append($('<div class="dialog-content" style="padding:10px; background-color:#fff">' + this.str + '</div>'))
            .append(
                    buttonPanel
                    );
    if (this.targetEl == 'body') {
        dialog.appendTo($('#modal'));
    }
    else {
        dialog.appendTo($(this.targetEl));
    }
    this.dialog = dialog;
    var self = this;
    for (var i = 0; i < this.buttons.length; i++) {
        var btn = self.buttons[i];
        $(this.dialog)
                .find('#' + btn.id).on('click', function (event) {
            self.clickHandler($(this).attr('id'), event);
            $(dialog).hide();
            $(darker).hide();
        });
    }

    $(dialog).show();
};

showDialogManyBtns.prototype.clickHandler = function (id, event) {
    if (typeof (this.callbacks[id]) !== "undefined") {
        this.callbacks[id]();
    }
    event.stopPropagation();
    event.preventDefault();
    return false;
};

ZOrder = function () {
    this.topMost = null;
    this.init = function () {
        var nodes = document.querySelectorAll('div'), maxZindex = -1;
        for (var i = 0; i < nodes.length; i++) {
            if (!isNaN(nodes[i].style.zIndex) && nodes[i].style.zIndex > maxZindex) {
                maxZindex = nodes[i].style.zIndex;
            }
        }
        this.topMost = maxZindex;
        return this.topMost;
    };

    this.bringToFront = function (el) {
        var topMost = this.init();
        if (el instanceof jQuery) {
            $(el).css({'z-index': topMost + 2});
            console.log(topMost);
        } else {
            el.style.zIndex = topMost + 2;
        }
    };
};

dialogWindow = function (url, targetEl, title, params) {
    var positionX = params && params.x || ~~(screen.width / 3),
            positionY = params && params.y || ~~(screen.height / 2),
            extract = params && params.extract,
            widget_id = params && params.widget_id,
            maxwidth = params && params.width || '570',
            maxwidth2 = parseInt(maxwidth) - 54,
            dialog_id = md5(url);
    extractSel = extract && params.extractSelector || '#modal';
    $('<div class="darker" id="darker_' + dialog_id + '"/>').prependTo(targetEl).show();
    var content = '';
    $.ajax(url, {'type': 'GET', 'async': false}).complete(function (response) {
        content = response.responseText;
    });
    var $content = '';
    if (extract) {
        var doc = $.parseHTML(content);
        $content = $(doc).find(extractSel).html();
    } else {
        $content = content;
    }

    var dialogWindow = $(
            '<div id="popup_' + dialog_id + '" class="panel popup" style="max-width:' + maxwidth + 'px;position:absolute;z-index:10001;left:' + positionX + 'px;top:' + positionY + 'px;"><div style="color:#fff" class="panel-heading"><b>' + title + '</b></div></div>')
            .append($('<div class="panel-body" style="max-width:' + maxwidth2 + 'px"></div>').append($content));
    if (widget_id) {
        $(dialogWindow).attr('id', widget_id);
    }
    if (params && params.id) {
        $(dialogWindow).attr('id', params.id);
    }
    if (targetEl == 'body') {
        dialogWindow.appendTo($('#modal'));
    }
    else {
        dialogWindow.appendTo($(targetEl));
    }
    $(dialogWindow).show();
    if (params.callback && typeof (params.callback) === "function") {
        params.callback(dialogWindow);
    }
    return dialog_id;

};




/*Array.prototype.last= function(){
 if (this.length > 0){
 return this[this.length-1];
 } else {
 return null;
 }
 }
 
 Array.prototype.head= function(){
 if (this.length > 0){
 return this[0];
 } else {
 return null;
 }
 }*/

String.prototype.replaceFromTo = function (from, to, replacement) {
    var fragment = this.substr(from, to - from);
    return this.replace(fragment, replacement);
};

Templater = function (url, data) {

    this.result = '';
    this.url = url;
    this.loop_stack = [];
    this.errors = [];
    this.loops = new Collection();
    this.variables = new Collection();
    this.data = data;
    this.html = '';
    this._substitute_vars = function () {
        var replacement = '';
        if (typeof (this.data[field]) != 'undefined') {
            replacement = this.data[field];
            if (format && /^\|\w\d$/.test(format)) {
                var format_type = /^\|(\w)(\d)$/.exec(format);
                if ('d' == format_type[1]) {
                    var precision = format_type[2];
                    replacement = parseFloat(replacement).toFixed(precision);
                }
            }
        }
        if (this.data[field] == null) {
            replacement = '';
        }
        while (html.indexOf('{{' + content + '}}') != -1) {
            this.html = this.html.replace('{{' + content + '}}', replacement);
        }
    }
};

Templater.prototype.fastLoad = function (url) {
    var _url = url || this.url;
    var reg_variable = /\{\{((.*?)(\|\w\d)*)\}\}/;
    var self = this;
    var result = '';

    $.ajax({'url': _url + '?rnd=' + Math.round(9999), 'type': 'GET', 'async': false}).complete(function (response) {

        var html = response.responseText;
        while (html.indexOf('\n') != -1) {
            html = html.replace('\n', '');
        }
        //var vars = /\{\{.*?\}\}/g.exec(html);
        while ((result = reg_variable.exec(html)) !== null) {
            var content = result[1];
            var field = result[2];
            var format = result[3];
            var replacement = '';
            if (typeof (self.data[field]) != 'undefined') {
                replacement = self.data[field];
                if (format && /^\|\w\d$/.test(format)) {
                    var format_type = /^\|(\w)(\d)$/.exec(format);
                    if ('d' == format_type[1]) {
                        var precision = format_type[2];
                        replacement = parseFloat(replacement).toFixed(precision);
                    }
                }
            }
            if (self.data[field] == null) {
                replacement = '';
            }
            while (html.indexOf('{{' + content + '}}') != -1) {
                html = html.replace('{{' + content + '}}', replacement);
            }
        }
        result = html;
    });

    return result;
}

Templater.prototype.parseLoops = function (level) {
    var _level = level || 0;
    if (level > 0) {
        var html = this.loop_stack.pop();
    } else {
        var html = this.html;
    }
    var loopresult = null;
    var reg_loop_start = /\[\[loop\|(.*?)\]\]/;
    var reg_variable = /\{\{((\w*?)(\.(\w*?)|\|\w\d)*|(\w*?)\((\w*?)\:(\w+)\))\}\}/;
    var reg_loop_params = /([\w\d_]*?)\|([\w\d_]+)\s*?as\s*?([\w\d_]+)\:?([\w\d\_]*?)$/;
    while ((loopresult = reg_loop_start.exec(html)) !== null) {
        if (loopresult[1]) {
            var l_index = this.loops.getNextId();
            var loop_tokens = reg_loop_params.exec(loopresult[1]);
            var pos_loop = parseInt(html.indexOf(loopresult[0])) + loopresult[0].length;
            var c_loop_body = html.substr(pos_loop, html.length);

            var regex_loop_end = new RegExp("(\\[\\[endloop\\|" + loop_tokens[1] + "\\]\\])");
            if (!regex_loop_end.test(c_loop_body)) {
                this.errors.push('Не найдено окончание цикла ' + loop_tokens[0]);
                return false;
            }
            var l_end_token = regex_loop_end.exec(c_loop_body);
            var end_body_pos = html.indexOf(l_end_token[0]) + l_end_token[0].length;
            if (loop_tokens.length > 0)
            {
                var loop_item = {
                    'name': loop_tokens[1],
                    'set': loop_tokens[2],
                    'index': typeof (loop_tokens[4]) != "undefined" && loop_tokens[4].length > 0 ? loop_tokens[3] : null,
                    'iterator': typeof (loop_tokens[4]) != "undefined" && loop_tokens[4].length > 0 ? loop_tokens[4] : loop_tokens[3],
                    'loop_body': html.substr(pos_loop, end_body_pos - pos_loop - l_end_token[0].length),
                    'loop_level': _level,
                    'start_offset': html.indexOf(loopresult[0]),
                    'end_offset': end_body_pos,
                    'code_replaces': 'C' + l_index
                };
                html = html.replaceFromTo(loop_item.start_offset, loop_item.end_offset, '{{{' + 'C' + l_index + '}}}');
                this.loop_stack.push(loop_item.loop_body);
                this.loops.addValue(loop_item, this.loops.getNextId());

                loop_item.loop_body = this.parseLoops(_level + 1);

            }
        }
    }
    return html;
};

Templater.prototype.parseVariables = function () {
    var reg_variable = /\{\{((.*?)(\|\w\d)*)\}\}/;
    var result = '';
    while ((result = reg_variable.exec(this.html)) !== null) {
        var variable_item = {
            'content': result[1],
            'field': result[2],
            'format': result[3]
        };
        this.variables.addValue(variable_item);
    }
};

Templater.prototype.parseLoopFunctions = function (loop_item, iterator) {
    var reg_function = /\{\{([\w\d_]*?)\(([\w\.]+)\:?(\w*?)\)\}\}/;
    var reg_variable = /[^\{](\{\{((\w*?)(\.(\w*?)|\|\w\d)*|(\w*?)\((\w*?)\:(\w+)\))\}\})[^\}]/;
    var loopfunction = null;
    var output = loop_item.loop_body;
    while (null !== (loopfunction = reg_function.exec(output))) {
        var replacement = '?';
        var function_name = loopfunction[1];
        var function_argument = loopfunction[2];
        if (typeof (window[function_name]) != "function") {
            this.errors.push("Идентификатор " + function_name + " не является функцией ")
        } else {
            if (typeof (loopfunction[3]) != "undefined" && loopfunction[3].length > 0) {
                var arg_array = [];
                var the_set_exists = typeof (this.data[loop_item.set]) != "undefined";
                var index_exists = typeof (this.data[loop_item.set][iterator][loopfunction[3]]) != "undefined";
                for (var i in this.data[loop_item.set]) {
                    var el = this.data[loop_item.set][i];
                    arg_array.push(el[loopfunction[3]]);
                }
                replacement = window[function_name](arg_array);
            } else {
                if (function_argument == loop_item.iterator) {
                    var arg_array = [];
                    var the_set_exists = typeof (this.data[loop_item.set]) != "undefined";
                    for (var i in this.data[loop_item.set]) {
                        var el = this.data[loop_item.set][i];
                        arg_array.push(el);
                    }
                    replacement = window[function_name](arg_array);
                } else {
                    replacement = window[function_name](function_argument);
                }
            }
        }
        output = output.replace(loopfunction[0], replacement);
    }
    return output;
}

Templater.prototype.parseLoopsVariables = function (loop_item, iterator) {
    var reg_variable = /[^\{](\{\{((\w*?)(\.(\w*?)|\|\w\d)*|(\w*?)\((\w*?)\:(\w+)\))\}\})[^\}]/;
    var l_var = null;
    var output = loop_item.loop_body;
    while ((l_var = reg_variable.exec(output)) !== null) {
        var replacement = '?';
        if (-1 === l_var[1].indexOf('.') && -1 === l_var[1].indexOf('|') && -1 === l_var[1].indexOf('(')) {
            if (l_var[2] == loop_item.name) {
                replacement = iterator;
            } else {
                if (l_var[3] == loop_item.iterator) {
                    var the_var_exists = typeof (this.data[loop_item.set][iterator]) != "undefined";
                    if (!the_var_exists) {
                        this.errors.push('Переменная ' + loop_item.set + '[' + iterator + ' ] ' + ' отсутствует в данных для заполнения шаблона');
                        return false;
                    } else {
                        replacement = this.data[loop_item.set][iterator];
                    }
                }
                else {
                    var the_var_exists = typeof (this.data[l_var[2]]) != "undefined" && this.data[l_var[2]];
                    if (!the_var_exists) {
                        this.errors.push('Переменная ' + l_var[2] + ' отсутствует в данных для заполнения шаблона');
                        return false;
                    } else {
                        replacement = this.data[l_var[2]];
                    }
                }
            }
        } else {
            if (l_var[1].indexOf('.') !== -1) {
                if (l_var[3] == loop_item.iterator) {
                    if (typeof (this.data[loop_item.set][iterator][l_var[5]]) == "undefined") {
                        this.errors.push('Поле ' + l_var[5] + ' объекта ' + l_var[3] + ' отсутствует в данных для заполнения шаблона');
                    }
                    replacement = this.data[loop_item.set][iterator][l_var[5]];
                }
            }
        }
        output = output.replace(l_var[1], replacement);
    }

    return output;
};

Templater.prototype.processLoops = function () {
    var reg_loop = /\{\{\{C(\d)\}\}\}/;
    var loopresult = null;
    var output = '';
    while ((loopresult = reg_loop.exec(this.html)) !== null) {
        var l_id = loopresult[1];
        var l_el = this.loops.getItemById(l_id);
        var the_set_exists = typeof (this.data[l_el.value.set]) != "undefined" && this.data[l_el.value.set];
        if (!the_set_exists) {
            this.errors.push('Массив ' + l_el.value.set + ' отсутствует в данных для заполнения шаблона');
            return false;
        }
        for (var i = 0; i < this.data[l_el.value.set].length; i++) {
            output = output.concat(this.parseLoopsVariables(l_el.value, i));
        }
        this.html = this.html.replace(loopresult[0], '');
    }
    return output;
};

Templater.prototype.load = function () {
    var _url = this.url;
    var self = this;
    $.ajax({'url': _url + '?rnd=' + Math.round(9999), 'type': 'GET', 'async': false}).complete(function (response) {
        var html = response.responseText;
        this.html = html;
        while (this.html.indexOf('\n') != -1) {
            this.html = html.replace('\n', '');
        }

    });
    return this.html;
};

Templater.prototype.loadFromString = function (str) {
    this.html = str;
    while (this.html.indexOf('\n') != -1) {
        this.html = html.replace('\n', '');
    }
}

showWidget = function (title, content, buttons, targetEl, params) {
    this.params = params || {};
    this.buttons = buttons || [{'id': 'OK', 'caption': 'OK', 'callback': function () {
            }}];
    this.title = title;
    this.str = content;
    this.targetEl = targetEl || 'body';
    this.callbacks = [];
};

showWidget.prototype.clickHandler = function (id) {
    if (typeof (this.callbacks[id]) !== "undefined") {
        this.callbacks[id]();
    }
};


showWidget.prototype.show = function () {

    var positionX = this.params.x || ~~(screen.width / 3);
    var positionY = this.params.y || ~~(screen.height / 2);
    var width = this.params.width || '250px';
    var buttonPanel = $('<div class="button-row" style="text-align:center;"></div>');
    for (var i = 0; i < this.buttons.length; i++) {
        var btn = this.buttons[i];

        this.callbacks[btn.id] = btn.callback;
        //$('#'+btn.id).on('click',function(){this.callbacks[1]()});
        var btnClass = btn.class || "btn";
        buttonPanel.append($(
                '<span style="padding-left:10px;padding-right:10px"><button class="' + btnClass + '" id="' + btn.id + '">' + btn.caption + '</button><span>'));
        //$('#'+btn.id).on('click',function(){  })
    }

    {
        var dialog = $(
                '<div id="widget" class="ui-widget .ui-widget-content ui-corner-all widget panel" style="position:absolute; z-index:999998; \n\
        width:' + width + '; max-height:555px; font-size:10pt; left:' + positionX + '; top:' + positionY + '"/>')
                .append($(
                        '<div class="panel-heading ui-widget-header" style="padding-right:5px"></div>')
                        .append('<span>'
                                + this.title
                                + '</span><span style="float:right;display:block;margin-top: -3px;cursor:pointer"><img src="images/window_close.png" class="widget-close"/></span>')
                        .click(function () {
                            $('.widget').remove();
                        }))
                .append($(
                        '<div class="panel-body dialog-content" style="padding:10px; background-color:#fff">' + this.str + '</div>'))
                .append(
                        buttonPanel
                        );
    }
    if (this.targetEl == 'body') {
        dialog.appendTo($('#modal'));
    }
    else {
        console.log($(this.targetEl));
        dialog.appendTo($(this.targetEl));
    }
    this.dialog = dialog;

    var self = this;
    for (var i = 0; i < this.buttons.length; i++) {
        var btn = self.buttons[i];

        $(this.dialog)
                .find('#' + btn.id).on('click', function () {
            self.clickHandler($(this).attr('id'));
            $(dialog).hide();

        });
    }

    //this.dialog.show();
};

showAjaxLoader = function (targetEl, params) {
    params = params || {'x': $(document).width() / 2, 'y': $(document).scrollTop() + screen.height / 2};
    var positionX = params.x || $(document).width() / 2;
    targetEl = targetEl || $('body');
    var title = params && params.title ? params.title : 'Подождите';
    var darker = $('<div class="darker"/>')
            .prependTo(targetEl)
            .show();
    var positionY = params.y;
    var dialog = $(
            '<div title="' + title + '" id="ajax_loader" class="panel" style="position:absolute; z-index:999999; width:90px; height:90px; left:' + positionX + 'px; top:' + positionY + 'px"/>')
            .append($('<div><img style="padding:27px;" src="images/ajax-loader.gif"/>'))
            .appendTo(targetEl);
    this.dialog = dialog;
    var self = this;
    $(dialog).show();

};

showMultiselect = function (list, targetEl, params) {
    targetEl = targetEl || $('body');
    var el_id = params && params.id || 'ms_name';
    var field = params && params.field || 'name';
    var el = $('<div class="multiselect"/>');
    var el_heading = $('<div class="multiselect-header"/>').append('<input type="text" id="' + el_id + '_filter" class="ms-filter"/ style="width:100px"/>');
    el.append(el_heading);
    var ul = $('<ul class="multiselect-list"/>');
    var el_body = $('<div class="multiselect-list-wrapper">')
    for (var i in list) {
        var item = $('<li data-val="' + list[i].id + '"><input name="chb_item_' + el_id + '[]" value="' + list[i].id + '">' + list[i][field] + '</li>');
        $(ul).append(item);
    }
    $(el_body).append(ul);
    $(el).append(el_body);
    $(div).css(
            {'top': $(targetEl).position().top + $(dockEl).height() - 10,
                'left': $(targetEl).position().left,
                'width': $(targetEl).width()
            }
    ).appendTo($(targetEl).parents('div').get(0)).show();
    $(el).show();
};

emptyFunction = function () {
};

removeDialog = function () {
    $('#dialog').remove();
    $('.darker').remove();
};


Collection = function (array) {
    var init_arr = array || [];
    this.items = init_arr;
    this.records = 0;
};

Collection.prototype.getArray = function () {
    return (this.items);
};

Collection.prototype.getItemById = function (id)
{
    for (var i in this.items) {
        if (this.items[i].id && parseInt(this.items[i].id) === parseInt(id))
        {
            return this.items[i];
        }
    }
};

Collection.prototype.addItem = function (item, id)
{
    var idx = id || this.items.length;
    this.items[idx] = item;
    this.records++;

};

Collection.prototype.addValue = function (value, id)
{
    var item = {'id': id, 'value': value};
    var idx = this.items.length;
    this.items[idx] = item;
    this.records++;
    return idx;

};

Collection.prototype.pushItem = function (item) {
    this.items.push(item);
    this.records++;
};


Collection.prototype.toString = function (format_object)
{
    if (!format_object) {
        return JSON.stringify(this.items);
    } else {
        var _items = {};
        for (var i in this.items)
        {
            if (null !== this.items[i]) {
                _items[i] = this.items[i];
            }
        }
        return JSON.stringify(_items);
    }
};

Collection.prototype.getNextId = function ()
{
    if (this.items.length == 0)
    {
        return 1;
    }
    var max_id = this.items[0].id;
    for (var i in this.items) {
        if (parseInt(this.items[i].id) > max_id) {
            max_id = this.items[i].id;
        }
    }
    return max_id + 1;
};

Collection.prototype.deleteItem = function (id)
{
    for (var i in this.items) {
        if (typeof (this.items[i]) == "object") {
            if (this.items[i].id == id) {
                this.items.splice(i, 1);
            }
        } else {
            if (i == id) {
                this.items.splice(i, 1);
            }
        }
    }
};


DataSource = function (url) {
    this.criteria = '';
    this.dataSet = [];
}

DataSource.prototype = Object.create(Collection.prototype);
DataSource.prototype.constructor = DataSource;

DataSource.prototype.connect = function () {
    var ajaxParams = {
        'url': 'index.php',
        'data': {
            'AjaxRequest': 'getTableDetails',
        }
    }
};




/*=======================================================*/
collectFormVariables = function (container, visible_only) {
    var visible_selector = visible_only ? ':visible' : '';
    var data = {};
    $(container).find('input' + visible_selector + ' ,select' + visible_selector + ', textarea' + visible_selector).each(function (i, e) {
        if ($(e).get(0).nodeName.toLowerCase() == 'input' || $(e).get(0).nodeName.toLowerCase() == 'textarea') {
            if ($(e).attr('type') && $(e).attr('type') == 'checkbox') {
                data[$(e).attr('name')] = $(e).prop('checked') ? 1 : 0
            } else {
                if ($(e).attr('type') && $(e).attr('type') == 'radio') {
                    var name = $(e).attr('name');
                    var el = $('input[name="' + name + '"]:checked');
                    if (el.length > 0) {
                        data[name] = el.val();
                    }
                } else {
                    data[$(e).attr('name')] = $(e).val();
                }
            }
        } else {
            data[$(e).attr('name')] = $(e).find('option:selected').attr('value');
        }
    });
    return data;
};

loadFormVariables = function (obj) {
    for (var e in obj) {
        var control = $('*[name="' + e + '"]');
        if (0 == $(control).length) {
            continue;
        }
        var cType = $(control).get(0).nodeName.toLowerCase();
        var cInputType = $(control).attr('type');
        if ('input' == cType || 'select' == cType || 'textarea' == cType) {
            if (cInputType == 'checkbox') {
                if (1 == parseInt(obj[e])) {
                    $(control).prop('checked', 'checked')
                } else
                {
                    $(control).prop('checked', null)
                }
            } else {
                if (cInputType == 'radio') {
                    $('*[name="' + e + '"][value="' + obj[e] + '"]').prop('checked', 'checked');
                } else {
                    $(control).val(obj[e]);
                }
            }

        }
    }
};



var md5 = new function () {
    var l = 'length',
            h = [
                '0123456789abcdef', 0x0F, 0x80, 0xFFFF,
                0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476
            ],
            x = [
                [0, 1, [7, 12, 17, 22]],
                [1, 5, [5, 9, 14, 20]],
                [5, 3, [4, 11, 16, 23]],
                [0, 7, [6, 10, 15, 21]]
            ],
            A = function (x, y, z) {
                return(((x >> 16) + (y >> 16) + ((z = (x & h[3]) + (y & h[3])) >> 16)) << 16) | (z & h[3])
            },
            B = function (s) {
                var n = ((s[l] + 8) >> 6) + 1, b = new Array(1 + n * 16).join('0').split('');
                for (var i = 0; i < s[l]; i++)
                    b[i >> 2] |= s.charCodeAt(i) << ((i % 4) * 8);
                return(b[i >> 2] |= h[2] << ((i % 4) * 8), b[n * 16 - 2] = s[l] * 8, b)
            },
            R = function (n, c) {
                return(n << c) | (n >>> (32 - c))
            },
            C = function (q, a, b, x, s, t) {
                return A(R(A(A(a, q), A(x, t)), s), b)
            },
            F = function (a, b, c, d, x, s, t) {
                return C((b & c) | ((~b) & d), a, b, x, s, t)
            },
            G = function (a, b, c, d, x, s, t) {
                return C((b & d) | (c & (~d)), a, b, x, s, t)
            },
            H = function (a, b, c, d, x, s, t) {
                return C(b ^ c ^ d, a, b, x, s, t)
            },
            I = function (a, b, c, d, x, s, t) {
                return C(c ^ (b | (~d)), a, b, x, s, t)
            },
            _ = [F, G, H, I],
            S = (function () {
                with (Math)
                    for (var i = 0, a = [], x = pow(2, 32); i < 64; a[i] = floor(abs(sin(++i)) * x))
                        ;
                return a
            })(),
            X = function (n) {
                for (var j = 0, s = ''; j < 4; j++)
                    s += h[0].charAt((n >> (j * 8 + 4)) & h[1]) + h[0].charAt((n >> (j * 8)) & h[1]);
                return s
            };
    return function (s) {
        var $ = B('' + s), a = [0, 1, 2, 3], b = [0, 3, 2, 1], v = [h[4], h[5], h[6], h[7]];
        for (var i, j, k, N = 0, J = 0, o = [].concat(v); N < $[l]; N += 16, o = [].concat(v), J = 0) {
            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                    for (k = 0; k < 4; k++, a.unshift(a.pop()))
                        v[b[k]] = _[i](
                                v[a[0]],
                                v[a[1]],
                                v[a[2]],
                                v[a[3]],
                                $[N + (((j * 4 + k) * x[i][1] + x[i][0]) % 16)],
                                x[i][2][k],
                                S[J++]
                                );
            for (i = 0; i < 4; i++)
                v[i] = A(v[i], o[i]);
        }
        ;
        return X(v[0]) + X(v[1]) + X(v[2]) + X(v[3]);
    }
};
