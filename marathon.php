<?php

require_once('mysql/mysql.class.php');

class MarathonImageWizard
{

    private $marathon;

    public function __construct($marathon)
    {
        $this->marathon = $marathon;
    }

    public function getImg()
    {
        $marathon  = $this->marathon;
        $img       = $marathon->template_path;
        $positions = array(
            1 => "Капитан",
            2 => "Заместитель капитана",
            3 => "Мудрец",
            4 => "Помощник рефери",
            5 => "Мотиваторник",
            6 => "Демотиваторник",
            7 => "Позитивник",
            8 => "Другое..."
        );
        $pinfo     = pathinfo($img, PATHINFO_EXTENSION);

        if (8 != $marathon->position)
        {
            $position = $positions[$marathon->position];
        }
        else
        {
            $position = $marathon->other_occupation;
        }
        switch ($pinfo)
        {
            case "png":
                $im = imagecreatefrompng($img);
                break;
            case "jpg":
                $im = imagecreatefromjpeg($img);
                break;
            case "gif":
                $im = imagecreatefromgif($img);
                break;
        }
        $navy_text = imagecolorallocate($im, 42, 80, 123);
        $red_text  = imagecolorallocate($im, 189, 81, 0);
        
        $italic    = 'fonts/Cuprum-Italic.ttf';
        $regular   = 'fonts/Cuprum.ttf';
        
        $vars      = array('position' => $marathon->position, 'days' => $marathon->getElapsed(), 'remains' => $marathon->getRemaining());

        $replaced_text = preg_replace_callback('#\{\$([^\}]+)\}#', function($matches) use ($vars) {
            if (isset($vars[$matches[1]]))
            {
                $value = $vars[$matches[1]];
                if ($matches[1] == 'days')
                {
                    $days_word = getNumEnding($value, array('день', 'дня', 'дней'));
                    return $value . ' ' . $days_word;
                }
                if ($matches[1] == 'remains')
                {
                    $days_word = getNumEnding($value, array('день', 'дня', 'дней'));

                    return $value . ' ' . $days_word;
                }
                return $vars[$matches[1]];
            }
        }, $marathon->text_template);
        $team = $marathon->team;
//$textSize = imagettfbbox(15, 0, $font_file, $text);
        imagefttext($im, MAIN_TEXT_FONT_SIZE, 0, 35, 10, $navy_text, $italic, $replaced_text, array());
        imagefttext($im, MAIN_TEXT_FONT_SIZE, 0, 35, 26,  $red_text, $italic, $position, array());
        if (!empty($marathon->team))
        {
            imagefttext($im, 15, 0, 120, 27, $red_text, $italic, $team, array());
        }
        imagefttext($im, 22, 0, 280, 24, $navy_text, $regular, $marathon->marathon_number, array());
        $new_fname      = mt_rand();
        $new_fname_full = IMAGE_PATH . $new_fname . '.' . $pinfo;
        imagepng($im, $new_fname_full);
        imagedestroy($im);
        return $new_fname_full;
    }

}

class Marathon
{

    public $id;
    public $length;
    public $marathon_start;
    public $marathon_name;
    public $position;
    public $team;
    public $other_occupation;
    public $resulting_image;
    public $text_template;
    public $record_created;
    public $dbconn;

    public function __construct()
    {
        $this->dbconn = new MySQL(true, LINES_DB_NAME, null, LINES_DB_USER, LINES_DB_PASS, 'UTF8');
    }

    public function getElapsed($time = null)
    {
        $time1 = !empty($time) ? date('d.m.Y H:i:s', strtotime($time)) : date('d.m.Y H:i:s');
        $time2 = !empty($this->id) ? $this->marathon_start : date('d.m.Y H:i:s');
        return (int) ((strtotime($time1) - strtotime($time2)) / 86400);
    }

    public function getRemaining($time = null)
    {
        $time1   = !empty($time) ? date('d.m.Y H:i:s', strtotime($time)) : date('d.m.Y H:i:s');
        $time2   = !empty($this->id) ? $this->marathon_start : date('d.m.Y H:i:s');
        $elapsed = (int) ((strtotime($time1) - strtotime($time2)) / 86400);
        return $this->length - $elapsed;
    }

    public function loadMarathon($id)
    {
        if (!empty($id) && is_numeric($id))
        {
            $row = $this->dbconn->QuerySingleRow('SELECT * FROM settings WHERE id = ' . intval($id));

            foreach ($row as $key => $value)
            {
                try
                {
                    $this->$key = $value;
                }
                catch (Exception $ex)
                {
                    
                }
            }
        }
        else
        {
            return null;
        }
    }

    public function loadFromArray($arr)
    {
        $this->id               = intval($arr['id']);
        $this->length           = intval($arr['ndays_count']);
        $this->template_path    = $arr['image_path'];
        $this->team             = $arr['team'];
        $this->marathon_name    = $arr['marathon_name'];
        $this->marathon_number  = $arr['marathon_number'];
        $this->position         = $arr['occupation'];
        $this->text_template    = $arr['text_template'];
        $this->other_occupation = $arr['other_occupation'];
        $this->marathon_start   = $arr['start_date'];
        $this->record_created   = date('Y-m-d');
    }

    public function getError()
    {
        echo $this->dbconn->Error();
        $this->dbconn->Kill();
    }

    public function saveMarathon()
    {
        $row = array();
        foreach ($this as $prop_key => $prop_val)
        {
            if (!is_object($prop_val) && !is_array($prop_val))
            {
                $row[$prop_key] = $prop_val;
            }
        }

        $id = isset($row['id']) && !empty($row['id']) ? $row['id'] : 0;
        if (!empty($id) && is_numeric($id))
        {
            foreach ($row as $key => &$value)
            {
                $row[$key] = MySQL::SQLValue($value);
            }
            //unset($row['id']);
            $this->dbconn->UpdateRows('settings', $row, array('id' => $id));
        }
        else
        {
            foreach ($row as $key => &$value)
            {
                $row[$key] = MySQL::SQLValue($value);
            }
            $id = $this->dbconn->InsertRow('settings', $row);
        }
        return $id;
    }

}

class Marathons
{

    private $dbconn;

    public function __construct()
    {
        $this->dbconn = new MySQL(true, LINES_DB_NAME, null, LINES_DB_USER, LINES_DB_PASS, 'UTF8');
    }

    public function getMarathons()
    {
        $sql_get_marathons = 'SELECT * FROM settings ORDER BY id';
        $rows              = $this->dbconn->Query($sql_get_marathons);
        return $rows;
    }

}

class HelperFunctions
{

    function listFiles($dir)
    {
        $dir      = "/tmp";
        $dh       = opendir($dir);
        $files    = array();
        while (false !== ($filename = readdir($dh)))
        {
            if (preg_match('#^.*?\.jpg$#'))
            {
                $files[] = $filename;
            }
        }
        return $files;
    }

}
